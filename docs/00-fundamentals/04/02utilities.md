# Extra utilities installeren

Moest je toch nog extra modules willen installeren voor de git versie voor Windows moet je deze natuurlijk wel op de juiste plaats zetten; Namelijk `C:\Program Files\Git\`: de locatie waar `git-bash.exe` te vinden zou zijn bij een standaard installatie. Moest deze daar toch niet staan is deze altijd te vinden door "open file location" te doen bij uw snelkoppeling naar git-bash.

binaries worden dan geplaatst in de map `mingw64\bin` waar "mingw64" de directoryroot is voor git

enkele nuttige binaries kunnen zijn:
* wget: https://eternallybored.org/misc/wget/
* Hugo static site generator: https://github.com/gohugoio/hugo/releases
* Xpdf: http://www.xpdfreader.com/download.html
* ExifTool: https://exiftool.org/
* make: (en veel andere): https://sourceforge.net/projects/ezwinports/files/
* nano (hernoem het bestand naar nano.exe): https://www.nano-editor.org/dist/win32-support/